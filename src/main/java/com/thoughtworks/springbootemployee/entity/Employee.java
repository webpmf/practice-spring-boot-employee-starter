package com.thoughtworks.springbootemployee.entity;

import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

public class Employee {
    private String name;
    private  Integer id;
    private String gender;
    private Integer age;
    private Integer salary;

    private Integer companyId;

    public Integer getCompanyId() {
        return companyId;
    }

    public Employee(String name, Integer id, String gender, Integer age, Integer salary,Integer companyId) {
        this.name = name;
        this.id = id;
        this.gender = gender;
        this.age = age;
        this.salary = salary;
        this.companyId = companyId;
    }

    public static List<Employee> init(){
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee("zhangshan",1,"male",16,20000,1));
        employees.add(new Employee("lisi",2,"female",16,20000,2));
        employees.add(new Employee("brand",4,"male",16,20000,3));
        employees.add(new Employee("dora",5,"male",16,20000,4));
        employees.add(new Employee("movin",6,"female",16,20000,5));

        return employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

}
