package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeRePository {
    private static List<Employee> employees = Employee.init();

    public static List<Employee> getEmployees() {
        return employees;
    }

    Employee getEmployee(Integer id) {
        return getEmployees().stream().
                filter(employee -> employee.getId().equals(id)).
                findFirst().orElse(null);
    }

    List<Employee> getEmployees(String gender) {
        return getEmployees().stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    Integer nextId() {
        int currentId = getEmployees().stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(1);
        return ++currentId;
    }

    void createEmployee(Employee employee) {
        employee.setId(nextId());
        getEmployees().add(employee);
    }

    static Employee getUpdateAndDelEmployee(Integer id) {
        Employee updateAndDelEmployee = getEmployees().stream()
                .filter(employeeItem -> employeeItem.getId().equals(id))
                .findFirst()
                .orElse(null);
        return updateAndDelEmployee;
    }

    List<Employee> getEmployeesByPageAndSize(Integer page, Integer size) {
        return getEmployees().stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}

