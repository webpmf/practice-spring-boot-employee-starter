package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository {
    private List<Company> companies = Company.initCompany();

    public List<Company> getCompanies() {
        return companies;
    }

    Company getCompany(Integer id) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
    }

    Integer nextId() {
        int currentId =  getCompanies().stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(1);
        return ++currentId;
    }

    Company getUpdateAndDelCompany(Integer id) {
        Company updateCompany =  getCompanies().stream()
                .filter(companyItem -> companyItem.getId().equals(id))
                .findFirst()
                .orElse(null);
        return updateCompany;
    }

    List<Employee> getEmployees(Integer companyId) {
        return EmployeeRePository.getEmployees().stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    void deleteCompany(Integer id) {
        Company deleteCompany = getUpdateAndDelCompany(id);
        Employee deleteEmployee = EmployeeRePository.getUpdateAndDelEmployee(id);
        getCompanies().remove(deleteCompany);
        EmployeeRePository.getEmployees().remove(deleteEmployee);
    }

    List<Company> getCompaniesByLimits(Integer page, Integer size) {
        return getCompanies().stream()
                .skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
