package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    public static final EmployeeRePository employeeRepository = new EmployeeRePository();

    @GetMapping()
    public List<Employee> getAll(){
        return employeeRepository.getEmployees();
    }

    @GetMapping(value = "/{id}")
    public Employee getEmployeeById(@PathVariable Integer id){
        return employeeRepository.getEmployee(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@RequestParam String gender){
        return employeeRepository.getEmployees(gender);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addEmployee(@RequestBody Employee employee) {
        employeeRepository.createEmployee(employee);
    }

    @PutMapping("/{id}")
    public void updateEmployeeById(@PathVariable Integer id, @RequestBody Employee employee) {
        Employee updateEmployee = employeeRepository.getUpdateAndDelEmployee(id);
        updateEmployee.setGender(employee.getGender() == null ? updateEmployee.getGender() : employee.getGender());
        updateEmployee.setAge(employee.getAge() == null ? updateEmployee.getAge() : employee.getAge());
        updateEmployee.setSalary(employee.getSalary() == null ? updateEmployee.getSalary() : employee.getSalary());
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable Integer id) {
        Employee deleteEmployee = employeeRepository.getUpdateAndDelEmployee(id);
        employeeRepository.getEmployees().remove(deleteEmployee);
    }

    @GetMapping(params = {"page","size"})
    public List<Employee> getByPageAndSize(@RequestParam Integer page,@RequestParam Integer size){
        return employeeRepository.getEmployeesByPageAndSize(page, size);
    }

}
