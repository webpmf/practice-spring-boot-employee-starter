package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    public static final CompanyRepository companyRepository = new CompanyRepository();

    @GetMapping
    public List<Company> getAllCompany(){
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Integer id){
        return companyRepository.getCompany(id);
    }

    @PostMapping
    public void addCompany(@RequestBody Company company) {
        company.setId(companyRepository.nextId());
        companyRepository.getCompanies().add(company);
    }

    @PutMapping("/{id}")
    public void updateCompanyById(@PathVariable Integer id, @RequestBody Company company) {
        Company updateCompany = companyRepository.getUpdateAndDelCompany(id);
        updateCompany.setName(company.getName() == null ? updateCompany.getName()  : company.getName());
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Integer companyId){
        return companyRepository.getEmployees(companyId);
    }

    @DeleteMapping("/{id}")
    public void deleteCompanyById(@PathVariable Integer id) {
        companyRepository.deleteCompany(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam Integer page, @RequestParam Integer size){
        return companyRepository.getCompaniesByLimits(page, size);
    }

}
